﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProteinTrackerServiceDemo
{
    public class EntryResponse
    {
        public int Age { get; set; }
        public string FullName { get; set; }
        public DateTime BirthDate { get; set; }
    }
}