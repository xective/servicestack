﻿using MongoDB.Driver;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProteinTrackerServiceDemo
{
    public class EntryService : Service
    {
        public object Any(Entry Request)
        {
            //var client = new MongoClient("mongodb://localhost:27017/ProteinDatabase");
            //var client = new MongoClient(new MongoClientSettings
            //{
            //    Server = new MongoServerAddress("localhost"),
            //    ClusterConfigurator = builder =>
            //    {
            //        builder.ConfigureCluster(settings => settings.With(serverSelectionTimeout: TimeSpan.FromSeconds(10)));
            //    }
            //});
            //var database = client.GetDatabase("UserDatabase");

            //database.CreateCollection("Incomes");
            return new EntryResponse {Age = 1, FullName = "Bim Bam", BirthDate = new DateTime(2014, 02, 21) };
        }
    }
}