﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProteinTrackerServiceDemo
{
    [Route("/entry/{Id}")]
    public class Entry : IReturn<EntryResponse>
    {
        public int Id { get; set; }
    }
}