﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Funq;

namespace ProteinTrackerServiceDemo
{
    public class Global : System.Web.HttpApplication
    {
        class UserGetterAppHost : AppHostBase
        {
            public UserGetterAppHost() : base("Protein Tracker", typeof(EntryService).Assembly)
            {
                    
            }

            public override void Configure(Funq.Container container)
            {
                //Configure our application
            }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            new UserGetterAppHost().Init();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}